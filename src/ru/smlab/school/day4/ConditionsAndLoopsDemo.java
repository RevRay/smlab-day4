package ru.smlab.school.day4;

import java.util.Scanner;

public class ConditionsAndLoopsDemo {
    public static void main(String[] args) {
        testIfAndSwitchConstructs();

        testLinearSearch();

        testLinearSearchIn2DArray();

        testIterateOverArray();
    }

    private static void testIterateOverArray() {
        int[] someNumbers = {34, -9, 10, 12, 60};

        //Пробуем перебрать массив и вывести каждый его элемент.
        //Рассмотрим несколько вариантов:
        //1. с циклом for со счетчиком (предпочтительный вариант, когда нам нужен индекс текущего элемента)
        for (int i = 0; i < someNumbers.length; i++) {
            System.out.println(someNumbers[i]);
        }
        //2. c циклом foreach (предпочтительный вариант, если нам не нужен доступ в цикле к текущему индексу)
        for (int i : someNumbers) {
            System.out.println(i);
        }
        //3. с циклом while
        int i = 0;
        while(i < someNumbers.length) {
            System.out.println(someNumbers[i]);
            i++;
        }
    }

    private static void testIfAndSwitchConstructs() {
        //Получаем пользовательский ввод в виде числа (порядкового номера дня недели),
        //и преообразуем в текстовый формат. Например 1 - Понедельник.
        //Ниже приведено два варианта решения этого задания:
        System.out.println("Добрый день! Введите порядковый номер дня недели в диапазоне от 1 до 7 для конвертации в строку:");
        Scanner sc = new Scanner(System.in);
        String userInput = sc.nextLine();
        //1. С использованием if-else
        parseWeekOfDayWithIfElse(userInput);
        //2. С использованием switch-case (предпочтительный вариант)
        parseWeekOfDayWithSwitch(userInput);
    }

    // демо линейного поиска в двухмерном массиве
    // с помощью вложенных циклов while & for, break, continue и меток (labels)
    public static void testLinearSearchIn2DArray() {
        //наш стартовый двухмерный массив.
        int[][] twoDimArray = {
                {1, 4, 5, 8},
                {30, 80, 90, 100, 1, 2},
                {30, 11, 5, 9},
                {90, 11, 2, 4},
        };
        //в нем мы должны найти индексы массивов, в которых содержится хотя бы одно значение 90
        int searchValue = 90;
        //мы попробуем сделать это двумя способами
        //1. с помощью циклов for со счетчиком (предпочтительный вариант)
        linearSearch2DArrayWithForLoop(twoDimArray, searchValue);
        //2. с помощью циклов while
        linearSearch2DArrayWithWhileLoop(twoDimArray, searchValue);
    }

    public static void linearSearch2DArrayWithForLoop(int[][] twoDimArray, int searchValue) {
        //myLoop: - это метка (label), по которой мы можем обратиться к конкретному циклу
        //в ситуации, когда мы находимся внутри нескольких вложенных циклов.
        myLoop:
        for (int i = 0; i < twoDimArray.length; i++) { //внешний цикл, в котором мы будем перебирать вложенные массивы
            for (int j = 0; j < twoDimArray[i].length; j++) { //внутренний цикл, в котором мы будем перебирать элементы текущего вложенного массива
                if (twoDimArray[i][j] == searchValue) {
                    System.out.println(String.format("Found 90 in array [%d] at index [%d]", i, j));
                    continue myLoop; //т.е. как только мы поняли, что в массиве есть хотя бы одно значение 90 - оставшиеся элементы нас не интересут, идем на следующую итерацию (следующий массив)
                    //break; можно заменить 'continue myLoop' на 'break', результат будет идентичен
                }
            }
        }
    }

    //смотри комментарии к коду метода linearSearch2DArrayWithForLoop - тут все по аналогии
    public static void linearSearch2DArrayWithWhileLoop(int[][] twoDimArray, int searchValue) {
        int i = 0;
        while (i < twoDimArray.length) {
            int j = 0;
            while (j < twoDimArray[i].length) {
                if (twoDimArray[i][j] == searchValue) {
                    System.out.println(String.format("Found 90 in array [%d] at index [%d]", i, j));
                    break;
                }
                j++;
            }
            i++;
        }
    }

    //Линейный поиск (linear search) - это поиск "втупую",
    //т.е. по очереди смотрим каждый элемент и проверяем - соответствует он искомому, или нет.
    //Справочно - имеет алгоритмическую сложность O(n).
    public static void testLinearSearch() {
        //искомое значение
        int target = -90;

        //массив, в котором мы будем осуществлять поиск
        int[] array = {20, 13, 23, 80, -90, 100, 80, 30};
        //индексы       0   1   2   3    4

        int counter = 0;
        while (counter < array.length) {
            System.out.println("iteration " + counter);
            if (array[counter] == target) { //если мы нашли наш элемент
                System.out.println(String.format("Found element %d at index %d", target, counter));
                break; //тогда пора закругляться и останавливать цикл. Элемент найден.
            }
            counter++;
        }
    }

    public static void parseWeekOfDayWithIfElse(String str) {
        if (str.equals("1")) {
            System.out.println("Понедельник");
        } else if (str.equals("2")) {
            System.out.println("Вторник");
        } else if (str.equals("3")) {
            System.out.println("Среда");
        } else if (str.equals("4")) {
            System.out.println("Четверг");
        } else if (str.equals("5")) {
            System.out.println("Пятница");
        } else if (str.equals("6")) {
            System.out.println("Суббота");
        } else if (str.equals("7")) {
            System.out.println("Воскресенье");
        } else {
            System.out.println("Введено неподдерживаемое значение дня недели. Формат ввода - цифра от 1 до 7.");
        }
    }

    public static void parseWeekOfDayWithSwitch(String str) {
        //поддерживаемые типы данных для switch - byte, short, char, int, String, Enum
        switch (str) {
            case "1":
                System.out.println("Понедельник");
                break; //без break при попадении в case "1" - мы провалимся в следующие case-ы и будем выполнять все подряд инструкции case-ов, до первого встреченного break.
            case "2":
                System.out.println("Вторник");
                break;
            case "3":
                System.out.println("Среда");
                break;
            case "4":
                System.out.println("Четверг");
                break;
            case "5":
                System.out.println("Пятница");
                break;
            case "6":
                System.out.println("Суббота");
                break;
            case "7":
                System.out.println("Воскресенье");
                break;
            default:
                System.out.println("Введено неподдерживаемое значение дня недели. Формат ввода - цифра от 1 до 7.");
        }
    }
}
